## About
This project is intended to get my friend interested in building his own discord bot and start learning some python.

## Requirements
  * [Python 3](https://www.python.org/)
  * [discord.py](https://pypi.org/project/discord.py/)
  * [python-dotenv](https://pypi.org/project/python-dotenv/)
