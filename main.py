import os
from dotenv import load_dotenv
import discord
from discord.ext import commands

load_dotenv()
bot = commands.Bot(command_prefix='!')

@bot.command()
async def whoareyou(ctx):
    await ctx.send('Hello <@!{}>, my name is {}! I was made to hopfully interest you into making a bot!'.format(ctx.author.id, ctx.me.nick))
@bot.command()
async def source(ctx):
    await ctx.send('You can find my source at <https://gitlab.com/Sandalot/py-bot>')

bot.run(os.getenv('DISCORD_TOKEN'))
